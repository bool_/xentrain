#pragma once
#pragma comment(lib, "detours/detours.lib")

#include <Windows.h>
#include "detours\detours.h"
#include <intrin.h>
#include "log.h"
#include "functions.h"

VOID Login(wchar_t* lpszUsername, wchar_t* lpszPassword, BYTE bServerId, BYTE bCharId);