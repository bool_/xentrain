#include "hacks.h"

INT nSpeedMultiplier = 2;

VOID WriteHackBytes(HACK hHack, BOOL bEnable) {
	if(bEnable) {
		memcpy((void*) hHack.dwAddress, (void*) hHack.szNewBytes, hHack.nSize);
	} else {
		memcpy((void*) hHack.dwAddress, (void*) hHack.szOriginalBytes, hHack.nSize);
	}
}

VOID WriteHookBytes(HOOK hkHook, VOID* lpvHook, BOOL bEnable) {
	if(bEnable) {
		*(UCHAR *) hkHook.dwAddress = 0xE9;
		*(UINT*)(hkHook.dwAddress + 1) = (DWORD) lpvHook - hkHook.dwAddress- 5;
		for(int i = 0; i < hkHook.nSize - 5; i++) {
			memcpy((VOID*) (hkHook.dwAddress + i + 5), (VOID*) "\x90", 1);
		}
	} else {
		memcpy((VOID*) hkHook.dwAddress, (VOID*) hkHook.szOriginalBytes, hkHook.nSize);
	}
}

DWORD HacksThreadProc() {
	Sleep(5000); // give the game some time to load
	memcpy((void*) dwZoomUnlock1, (void*) "\x0C\x0C", 2);
	memcpy((void*) dwZoomUnlock2, (void*) "\x0C\x0C", 2);
	return 0;
}

VOID SetSpeedMultiplier(INT iMultiplier) {
	nSpeedMultiplier = iMultiplier;
}

VOID __declspec(naked) SpeedhackCodeCave() { 
	__asm { 
		pushfd
		imul eax,[nSpeedMultiplier]
		popfd
		mov [esp+0x14],eax
		fild dword ptr [esp+0x14]
		jmp hkSpeedhack.dwReturn
	} 
}