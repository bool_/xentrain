#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#include "dialog.h"

HWND hwndIdLabel;
HWND hwndMoneyLabel;
HWND hwndStatusLabel;

CHAR* apszStates[11] = {
"Not Botting", // 0
// 1-10 chestbot states
"Entering Black Mountain Path", // 1
"Loading Black Mountain Path", // 2
"Teleporting to Black Sovereign", // 3
"Entering Black Sovereign Assembly", // 4
"Loading Black Sovereign Assembly", // 5
"Looting chests", // 6
"Loading Saint's Haven", // 7
};

DWORD PulseThreadProc(HWND hwndDlg) {
	while(true) {
		INT nMoney;
		if(ReadInt(&nMoney, (LPVOID) dwMoneyBase, dwMoneyOfs)) {
			char string[12];
			char string2[12];
			sprintf_s(string, "%d", nMoney);
			GetWindowText(hwndMoneyLabel, string2, 12);
			if(strcmp(string, string2) != 0) {
				SetWindowText(hwndMoneyLabel, string);
			}
		}
		char string1[64];
		char* pszStatus = apszStates[GetChestbotStatus()];
		GetWindowText(hwndStatusLabel, string1, 64);
		if(strcmp(pszStatus, string1) != 0) {
			SetWindowText(hwndStatusLabel, pszStatus);
		}
	}
	return 0;
}

INT_PTR CALLBACK TrainerDialogCallback(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch(uMsg) {
	case WM_INITDIALOG:
		hwndMoneyLabel = GetDlgItem(hwndDlg, MONEY_STATIC);
		hwndStatusLabel = GetDlgItem(hwndDlg, STATUS_STATIC);
		hwndIdLabel = GetDlgItem(hwndDlg, ID_STATIC);
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) HacksThreadProc, NULL, 0, NULL);
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) PulseThreadProc, hwndDlg, 0, NULL);
		CheckRadioButton(hwndDlg, SPEED2X_RADIO, SPEED5X_RADIO, SPEED2X_RADIO);
		RegisterHotKey(hwndDlg, CHESTBOT_CHECK, NULL, VK_F6);
		RegisterHotKey(hwndDlg, ESC_ROPE_BUTTON, NULL, VK_F12);
		break;

	case WM_HOTKEY:
		switch(HIWORD(lParam)) {
		case VK_F6:
			if(!IsDlgButtonChecked(hwndDlg, CHESTBOT_CHECK)) {
				StartChestbot();
				CheckDlgButton(hwndDlg, CHESTBOT_CHECK, BST_CHECKED);
			} else {
				StopChestbot();
				CheckDlgButton(hwndDlg, CHESTBOT_CHECK, BST_UNCHECKED);
			}
		case VK_F12:
			SendAbandonStage(0, 0);
			break;
		}
	case WM_COMMAND:
		if(HIWORD(wParam) == BN_CLICKED) {
			switch(LOWORD(wParam)) {
			case NOCLIP_CHECK:
				if(IsDlgButtonChecked(hwndDlg, NOCLIP_CHECK)) {
					WriteHackBytes(hNoclip, true);
				} else {
					WriteHackBytes(hNoclip, false);
				}
				break;

			case INF_JUMP_CHECK:
				if(IsDlgButtonChecked(hwndDlg, INF_JUMP_CHECK)) {
					WriteHackBytes(hInfiniteJump, true);
				} else {
					WriteHackBytes(hInfiniteJump, false);
				}
				break;
			case NO_CD_CHECK:
				if(IsDlgButtonChecked(hwndDlg, NO_CD_CHECK)) {
					WriteHackBytes(hNoCooldown, true);
				} else {
					WriteHackBytes(hNoCooldown, false);
				}
				break;
			case SPEED_CHECK:
				if(IsDlgButtonChecked(hwndDlg, SPEED_CHECK)) {
					WriteHookBytes(hkSpeedhack, (VOID*) SpeedhackCodeCave, true);
				} else {
					WriteHookBytes(hkSpeedhack, (VOID*) SpeedhackCodeCave, false);
				}
				break;
			case SPEED2X_RADIO:
				if(IsDlgButtonChecked(hwndDlg, SPEED2X_RADIO)) {
					SetSpeedMultiplier(2);
				}
				break;
			case SPEED3X_RADIO:
				if(IsDlgButtonChecked(hwndDlg, SPEED3X_RADIO)) {
					SetSpeedMultiplier(3);
				}
				break;
			case SPEED4X_RADIO:
				if(IsDlgButtonChecked(hwndDlg, SPEED4X_RADIO)) {
					SetSpeedMultiplier(4);
				}
				break;
			case SPEED5X_RADIO:
				if(IsDlgButtonChecked(hwndDlg, SPEED5X_RADIO)) {
					SetSpeedMultiplier(5);
				}
				break;
			case CHESTBOT_CHECK:
				if(IsDlgButtonChecked(hwndDlg, CHESTBOT_CHECK)) {
					StartChestbot();
				} else {
					StopChestbot();
				}
				break;
			case ESC_ROPE_BUTTON:
				SendAbandonStage(0, 0);
				break;
			case LOGIN_BUTTON:
				Login(L"anthonysnew5", L"anthony123", 4, 0);
				break;
			}
		}
		return true;
		break;
	}
	return DefWindowProc(hwndDlg, uMsg, wParam, lParam);
}