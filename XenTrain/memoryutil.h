#pragma once

#include <stdarg.h>
#include <Windows.h>

BOOL ReadFloat(FLOAT* pfValue, LPCVOID lpcvBase, ...);
BOOL WriteFloat(FLOAT fValue, LPCVOID lpcvBase, ...);
BOOL ReadInt(INT* pfValue, LPCVOID lpcvBase, ...);
BOOL ReadString(LPWSTR* pfValue, LPCVOID lpcvBase, ...);