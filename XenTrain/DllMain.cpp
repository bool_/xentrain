#include <Windows.h>

#include "resource.h"
#include "dialog.h"

void DialogThreadProc(HINSTANCE hInstance) {
    DialogBox(hInstance, MAKEINTRESOURCE(TRAINER_DIALOG), NULL, (DLGPROC) TrainerDialogCallback);
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {
	switch(fdwReason) {
	case DLL_PROCESS_ATTACH:
		AllocConsole();
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) DialogThreadProc, hinstDLL, 0, NULL);
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return true;
}
	