#pragma once

#include <Windows.h>
#include "hack_constants.h"

const lpfnSendAbandonStage SendAbandonStage = (lpfnSendAbandonStage) dwSendAbandonStage;
const lpfnSendSelectedServer SendSelectedServer = (lpfnSendSelectedServer) dwSendSelectedServer;
const lpfnSendLogin SendLogin = (lpfnSendLogin) dwSendLogin;
const lpfnSendSelectedChannel SendSelectedChannel = (lpfnSendSelectedChannel) dwSendSelectedChannel;

HWND WINAPI FindProcessWindow(LPCSTR lpcszWindowClass, DWORD dwProcessId);

VOID ClickChest(DWORD dwChestId);
VOID SendStartStage(BYTE bStageId, DWORD dwDifficulty);
VOID DropItem(DWORD dwSlot, DWORD dwCount, DWORD dwTimestamp);
VOID SendSelectChar(BYTE bCharId, BYTE bUnk, DWORD* lpdwUnk);