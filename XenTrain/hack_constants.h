#pragma once

#include <Windows.h>

struct HACK {
   DWORD dwAddress;
   char* szOriginalBytes;
   char* szNewBytes;
   INT nSize;
}; 

struct HOOK {
   DWORD dwAddress;
   DWORD dwReturn;
   char* szOriginalBytes;
   INT nSize;
}; 

const HOOK hkSpeedhack		= { 0x005A0836, 0x005A083E, "\x89\x44\x24\x14\xDB\x44\x24\x14", 8 };

// hacks
const HACK hInfiniteJump	= { 0x005502B8, "\x8B\x84\x39\x74\x1A\x00\x00", "\xB8\x01\x00\x00\x00\x90\x90", 7 };
const HACK hNoclip			= { 0x00570860, "\x81\xEC\xA0\x00\x00\x00",		"\xC2\x1C\x00\x90\x90\x90",		6 };
const HACK hNoCooldown		= { 0x00660C54, "\xD9\x87\xD8\x01\x00\x00",		"\x90\x90\x90\x90\x90\x90",		6 };

// hack addresses
const DWORD dwZoomUnlock1 = 0x00640FD6;
const DWORD dwZoomUnlock2 = 0x00640FF8;

// hook addresses
const DWORD dwSpeedhack = 0x005A0836;
const DWORD dwSpeedhackReturn = 0x005A083E;
const DWORD dwDebugString = 0x006A0790;

// function addresses
const DWORD dwSendAbandonStage = 0x00472590;
const DWORD dwSendStartStage = 0x00401140;
const DWORD dwClickChest = 0x0053E100;
const DWORD dwSendSelectDungeonInfo = 0x0083C770;
const DWORD dwSendSelectedServer = 0x00464390;
const DWORD dwSendLogin = 0x00471020;
const DWORD dwSendSelectChar = 0x00464080;
const DWORD dwSendSelectedChannel = 0x00464400;

// function prototypes
typedef void (__cdecl* lpfnSendAbandonStage)(BYTE bUnk, BYTE bUnk2); 
typedef void (__cdecl* lpfnSendSelectedServer)(BYTE bServerId);
typedef void (WINAPI* lpfnSendLogin)(wchar_t* lpszUsername, wchar_t* lpszPassword);
typedef void (__cdecl* lpfnSendSelectedChannel)(DWORD dwChannel);

// pointers
const DWORD dwCameraBase = 0x00f59214;
const DWORD dwOfsZoom = 0x0bdc;

const DWORD dwCharBase = 0x00f4fac0;
const DWORD dwOfsCharX = 0x08AC;
const DWORD dwOfsCharY = dwOfsCharX + 4;
const DWORD dwOfsCharZ = dwOfsCharY + 4;
const DWORD dwOfsCharUnk = 0x1F1C;
const DWORD dwOfsCharHP = 0x0068;

const DWORD dwItemBase = 0x0125C278;
const DWORD dwOfsItem1 = 0x0008;
const DWORD dwOfsItemCount = 0x0034;

const DWORD dwMoneyBase = 0x012BF738;
const DWORD dwMoneyOfs = 0x0068;