#pragma once

#include <Windows.h>
#include <WindowsX.h>

#include "hacks.h"
#include "chestbot.h"
#include "resource.h"
#include "autologin.h"

INT_PTR CALLBACK TrainerDialogCallback(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);