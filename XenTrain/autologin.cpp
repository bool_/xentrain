#include "autologin.h"

VOID DebugStringLogin(wchar_t* fmt, ...);

typedef void (*tDebugStringLogin)(__in const wchar_t* buf);
tDebugStringLogin DebugStringTrampLogin, DebugStringDetourLogin;

BYTE bSID;
BYTE bCID;

VOID Login(wchar_t* lpszUsername, wchar_t* lpszPassword, BYTE bServerId, BYTE bCharId) {
	DebugStringDetourLogin = (tDebugStringLogin) dwDebugString;
	DebugStringTrampLogin = (tDebugStringLogin) DetourFunction((PBYTE) DebugStringDetourLogin, (PBYTE) DebugStringLogin);
	bSID = bServerId;
	bCID = bCharId;
	SendLogin(lpszUsername, lpszPassword);
}

VOID DebugStringLogin(wchar_t* fmt, ...) {
	if(wcscmp(fmt, L"CDnLoginTask::OnRecvLoginServerList") == 0) {
		SendSelectedServer(bSID);
	} else if(wcscmp(fmt, L"CDnLoginTask::OnRecvLoginCharList") == 0) {
		DWORD dwShit = 0;
		SendSelectChar(bCID, 0, &dwShit); // TODO figure out what kind of dirty hax second param is
	} else if(wcscmp(fmt, L"CDnLoginTask::OnRecvLoginChannelList") == 0) {
		SendSelectedChannel(1);
		DetourRemove((PBYTE) DebugStringTrampLogin, (PBYTE) DebugStringLogin);
	}
}