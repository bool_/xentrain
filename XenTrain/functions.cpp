#include "functions.h"

HWND WINAPI FindProcessWindow(LPCSTR lpcszWindowClass, DWORD dwProcessId) {
	DWORD dwWindowId;
	CHAR pszClassName[200];
	HWND hWnd;
	hWnd = GetTopWindow (NULL);
	
	while(hWnd != NULL) {
		if(GetClassName(hWnd, pszClassName, 200) > 0)
			if(lstrcmpiA(lpcszWindowClass, pszClassName) == 0)
				if(GetWindowThreadProcessId(hWnd, &dwWindowId))
					if(dwWindowId == dwProcessId)
						return hWnd;
		hWnd = GetNextWindow(hWnd, GW_HWNDNEXT);
	}
	return NULL;
}

// TODO figure out why this doesn't work
DWORD dwSendFieldPacket = 0x0092C350;
DWORD dwClass = 0x0F007D70;
VOID SendFieldPacket(DWORD dwOpcode1, DWORD dwOpcode2, BYTE* lpbData, DWORD dwLength) {
	__asm {
		push dwLength
		push lpbData
		push dwOpcode2
		push dwOpcode1
		push dwClass
		xor al,al
		mov ecx,dwLength
		call dwSendFieldPacket
	}
}

DWORD dwDropItem = 0x0048FC40; // (2, slot, count, timestamp, ptr_unk)
VOID DropItem(DWORD dwSlot, DWORD dwCount, DWORD dwTimestamp) {
	__asm {
		mov esi,[0x012BF738]
		push 0x00
		push dwTimestamp
		push dwCount
		push dwSlot
		push 0x02
		call dwDropItem
	}

}

VOID ClickChest(DWORD dwChestId) {
	__asm {
		mov edi,0x03
		push 0x00
		mov edx,0x00
		mov ecx,dwChestId
		call dwClickChest
		add esp,0x04
	}
}

VOID SendStartStage(BYTE bStageId, DWORD dwDifficulty) {
	__asm {
		mov cl,bStageId
		push 0x00 // seems to always be 0 who gives a fuck
		push dwDifficulty
		xor edx,edx
		call dwSendStartStage
		add esp,0x08
	}
}

VOID SendSelectChar(BYTE bCharId, BYTE bUnk, DWORD* lpdwUnk) {
	__asm {
		mov eax,lpdwUnk
		push bUnk
		push bCharId
		call dwSendSelectChar
		add esp,0x08
	}
}