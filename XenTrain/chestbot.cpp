#include "chestbot.h"

// chestbot constants
FLOAT fTownPortalX = -377.0;
FLOAT fTownPortalZ = 10849.0;
FLOAT fDungeonPortalX = 1188.0;
FLOAT fDungeonPortalZ = 1808.0;
FLOAT fChestX = 1300.0;
FLOAT fChestZ = 1844.0;

INT nState = 0;

INT GetChestbotStatus() {
	return nState;
}

DWORD TeleportThreadProc(TELEPORT* tTeleport) {
	Sleep(tTeleport->nDelay);
	WriteFloat(tTeleport->fTeleportX, (LPVOID) dwCharBase, dwOfsCharX);
	WriteFloat(tTeleport->fTeleportZ, (LPVOID) dwCharBase, dwOfsCharZ);
	return 0;
}

TELEPORT tTeleport;

VOID DebugString(wchar_t* fmt, ...);

typedef void (*tDebugString)(__in const wchar_t* buf);
tDebugString DebugStringTramp, DebugStringDetour;

VOID StartChestbot() {
	nState = 1;
	DebugStringDetour = (tDebugString) dwDebugString;
	DebugStringTramp = (tDebugString) DetourFunction((PBYTE) DebugStringDetour, (PBYTE) DebugString);
	tTeleport.fTeleportX = fTownPortalX;
	tTeleport.fTeleportZ = fTownPortalZ;
	tTeleport.nDelay = 2000;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) TeleportThreadProc, (LPVOID) &tTeleport, 0, NULL);
}

VOID StopChestbot() {
	nState = 0;
	DetourRemove((PBYTE) DebugStringTramp, (PBYTE) DebugString);
}

DWORD ChestOpenThreadProc() {
	nState = 6;
	WriteFloat(fChestX, (LPVOID) dwCharBase, dwOfsCharX);
	WriteFloat(fChestZ, (LPVOID) dwCharBase, dwOfsCharZ);
	Sleep(1000);
	ClickChest(0x48F); // leftmost
	ClickChest(0x49B); // 2nd
	ClickChest(0x49F); // 3rd
	ClickChest(0x4A6); // rightmost
	ClickChest(0x49A); // royal chest on top
	FLOAT fItemCount = 0;
	ReadFloat(&fItemCount, (LPVOID) dwItemBase, dwOfsItem1, dwOfsItemCount);
	while(fItemCount == 0) // wait for chests to open
		ReadFloat(&fItemCount, (LPVOID) dwItemBase, dwOfsItem1, dwOfsItemCount);
	while(fItemCount != 0) // wait till all items are looted
		ReadFloat(&fItemCount, (LPVOID) dwItemBase, dwOfsItem1, dwOfsItemCount);
	if(nState == 6) {
		SendAbandonStage(0,0);
		nState = 7;
	}
	return 0;
}

VOID DebugString(wchar_t* fmt, ...) { // called for packet events (some odd form of debug info)
	// TODO event queueing to avoid creating new threads
	if(nState == 0)
		return;
	wchar_t buf[4096];
	va_list va;
	va_start(va, fmt);
	wvsprintfW(buf, fmt, va);
	Log("DebugString: %x -> %S\n", (DWORD) _ReturnAddress(), buf);
	va_end(va);
	if(wcscmp(fmt, L"CDnPartyTask::OnRecvSelectStage") == 0) { // gate triggered
		if(nState == 1) {
			Log("Sending start stage\n");
			SendStartStage(-1, 0);
			nState = 2;
		}
	} else if(wcscmp(fmt, L"CGameClientSession::OnRecvRoomSyncStart") == 0) { // map changed
		if(nState == 2) {
			Log("TELEPORTING TO DUNGEON!\n");
			nState = 3;
			tTeleport.fTeleportX = fDungeonPortalX;
			tTeleport.fTeleportZ = fDungeonPortalZ;
			tTeleport.nDelay = 2000;
			CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) TeleportThreadProc, (LPVOID) &tTeleport, 0, NULL);
		} else if(nState == 5) {
			CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) ChestOpenThreadProc, NULL, 0, NULL);
		}
	} else if(wcscmp(fmt, L"SendSelectDungeonInfo") == 0) { // dungeon window events
		if (nState == 3) {
			nState = 4;
		} else if(nState == 4) {
			Log("Entering dungeon\n");
			nState = 5;
			SendStartStage(2, 4);
		}
	} else if(wcscmp(fmt, L"SendCompleteLoading") == 0) { // village loaded
		Log("Starting over\n");
		nState = 1;
		tTeleport.fTeleportX = fTownPortalX;
		tTeleport.fTeleportZ = fTownPortalZ;
		tTeleport.nDelay = 2000;
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) TeleportThreadProc, (LPVOID) &tTeleport, 0, NULL);
	}
}