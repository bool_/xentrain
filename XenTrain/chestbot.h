#pragma once
#pragma comment(lib, "detours/detours.lib")

#include <Windows.h>
#include "detours\detours.h"
#include <intrin.h>
#include "resource.h"
#include "log.h"
#include "memoryutil.h"
#include "functions.h"

struct TELEPORT {
   FLOAT fTeleportX;
   FLOAT fTeleportZ;
   INT nDelay;
}; 

VOID StopChestbot();
VOID StartChestbot();
INT GetChestbotStatus();
