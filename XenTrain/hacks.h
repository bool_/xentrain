#pragma once
#pragma comment(lib, "detours/detours.lib")

#include <Windows.h>
#include "detours\detours.h"
#include "hack_constants.h"
#include "log.h"

VOID WriteHackBytes(HACK hHack, BOOL bEnable);
VOID WriteHookBytes(HOOK hkHook, VOID* lpvHook, BOOL bEnable);
VOID SetSpeedMultiplier(INT iMultiplier);
VOID SpeedhackCodeCave();

DWORD HacksThreadProc();