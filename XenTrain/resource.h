//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by XenTrain.rc
//
#define TRAINER_DIALOG                  101
#define TELEPORT_DIALOG                 102
#define SPEED_CHECK                     1003
#define NOCLIP_CHECK                    1004
#define INF_JUMP_CHECK                  1006
#define HACKGRP_STATIC                  1007
#define NO_CD_CHECK                     1008
#define SETTINGS_STATIC                 1013
#define SPEED2X_RADIO                   1024
#define SPEED3X_RADIO                   1025
#define SPEED4X_RADIO                   1027
#define SPEED5X_RADIO                   1028
#define BOTSGRP_STATIC                  1034
#define CHESTBOT_CHECK                  1035
#define IDC_BUTTON1                     1036
#define INFOGRP_STATIC                  1038
#define ID_STATIC                       1039
#define MONEY_STATIC                    1040
#define STATUS_STATIC                   1041
#define ESC_ROPE_BUTTON                 1042
#define DEBUG_BUTTON                    1046
#define LOGIN_BUTTON                    1047
#define CHARS_COMBO                     1049
#define DRP_BUTTON                      1050
#define IDC_BUTTON3                     1051

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1052
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
